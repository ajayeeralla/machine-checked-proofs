(************************************************************************)
(* Copyright (c) 2015-2019, Ajay Kumar Eeralla                          *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)    
Require Export dsaxioms.
Section dh_auth.
(** This library contains proof of the responder's authentication of the initior of authenticated version of the DH protocol (STS protocol). *)

(** # <b> Authenticated version of the DH protocol:</b>
</br>
Group description G and g are generated honestly, according to a randomized algorithm and are made public.
Public key, secret key pairs (pkA , skA ) and (pkB , skB ) are generated honestly for both A and B and pkA , pkB are made public.
<ul>
<li> The Initiator generates a random <i> a </i> in <i> Z<sub>|g|</sub> </i> and sends <i> g<sup>a</sup>. </li> 
<li> The Responder receives <i> g <sup> a </sup> </i>, generates a random <i> b </i> in <i> Z<sub>|g|</sub></i> and
sends <i> < g<sup>b</sup>, sign(skB, g<sup> b </sup>, g<sup>a</sup>) > </i>, and computes <i> (g <sup> a </sup>)<sup> b </sup> </i>. </li>
<li> The Initiator receives <i> < g<sup> b </sup> , sign(skB , g<sup> b</sup>, g<sup>a</sup> ) > </i>, verifies the signature, computes (g<sup> b </sup>)<sup> a </sup>, and sends <i> sign(skA , g <sup> a </sup>, g <sup>  b </sup> ) </i>.
<li> The Responder receives sign (skA, < g <sup> a </sup>, g <sup> b </sup> >), verifies the signature, and output <i> acc </i>. </li>
</ul>
#
 *)
Notation "( x , y , .. , z )" := (pair .. (pair x y) .. z).

(** Authentication is modeled as indistinguishability of two protocols, one in which the oracle reveals [TRue] if the given session is completed but no matching session (there exists an attack), and the one in which the oracle always reveals [FAlse]. 
We use [new] for [TRue] and [acc] for [FAlse] as those are of type message. *)
(** Protocol [Pi1] reveals [acc] always. *)

(** Frame [phi10] represents initial knowledge of the attacker. *)
Variable n: nat.
Variables n1 n2: nat.
Variables nA nB: nat.
Variables na nb: nat. 
Variables c1 c2: nat. 

(** We assume that the agents are pair wise distinct **)
Axiom pair_dist1: A #? B ## FAlse.
Axiom pair_dist2: A #? Q1 ## FAlse.
Axiom pair_dist3: A #? Q2 ## FAlse.
Axiom pair_dist4: B #? Q1 ## FAlse.
Axiom pair_dist5: B #? Q2 ## FAlse.
Axiom pair_dist6: Q1 #? Q2 ## FAlse.

(** Check if the recieved agent id is valid *)

Definition check (Q:message):= ((Q#?A) or (Q#?B) or (Q#?Q1) or (Q#?Q2)).
Definition pkey (Q:message):= (If (Q#?A) then (pk nA)
                               else (If (Q#?B) then (pk nB)
                                     else (If (Q#?Q1) then (pk c1)
                                           else (If (Q#?Q2) then (pk c2)
                                                 else O)))).
Definition tau n (t:message) := match n, t with
                                | 1, t => (pi1 t) 
                                | 2, t => (pi1 (pi2 t))
                                | 3, t => (pi2 (pi2 t))
                                | _, _ => O
                                end.

Definition phi10 := [msg (G n), msg (g n), msg A, msg (pk nA), msg B, msg (pk nB), msg Q1, msg (pk c1), msg Q2, msg (pk c2)].
Definition mphi10 := (conv_mylist_listm phi10).

Notation " x '^^' y " := (exp (G x) (g x) (r y)) (at level 0).

Definition gr (n n':nat) := (exp (G n) (g n) (r n')).
Definition x1 := (f mphi10).
Definition qa0 :=   (If ((to x1) #? (i 1)) & ((act x1)#? new) & (check (m x1)) then (A, ((m x1), n ^^ na))  else O).


(** Frame [phi11] represents attacker's knowledge during execution of the protocol. *)

Definition phi11 := phi10 ++ [msg (A, ((m x1), n ^^ na))].
Definition mphi11 := (conv_mylist_listm phi11).
Definition x2 := (f mphi11).
Definition qa1 := (If ((to x2)#?(i 2))& (check (tau 1 (m x2))) & ((tau 2 (m x2)) #? B) then (n ^^ nb, (sign (sk nB) ((tau 1 (m x2)), (n ^^ nb, (tau 3 (m x2)))) (r n1))) else O).
Definition qa0_s :=   (If ((to x1) #? (i 1)) & ((act x1)#? new) & (check (m x1)) then qa1 else O).

(** Frame [phi12] also represents attackers's knowledge during execution of the protocol. *)
 Definition phi12:= phi11 ++ [msg (n ^^ nb, (sign (sk nB) ((tau 1 (m x2)), (n ^^ nb, (tau 3 (m x2)))) (r n1)))].

Definition mphi12 := (conv_mylist_listm phi12).
Definition x3 := (f mphi12).
   
Definition qa2 :=  (If ((to x3) #?(i 1)) & (ver (pkey (m x1)) (A, ((pi1 (m x3)), n ^^ na)) (pi2 (m x3))) then (sign (sk nA) ((m x1), (n ^^ na, (pi1 (m x3)))) (r n2)) else O).



Definition qa1_s :=   (If ((to x2)#?(i 2))& (check (tau 1 (m x2))) & ((tau 2 (m x2)) #? B) then qa2 else O). 
 Definition qa0_ss :=   (If ((to x1) #? (i 1)) & ((act x1)#? new) & (check (m x1)) then qa1_s else O).
(** Frame [phi13] *) 
 Definition phi13 := phi12 ++ [msg (sign (sk nA) ((m x1), (n ^^ na, (pi1 (m x3)))) (r n2))].
 Definition mphi13 := (conv_mylist_listm phi13).
 Definition x4 := (f mphi13).
  
Definition qa3 :=  (If ((to x4)#? (i 2)) & (ver (pkey (tau 1 (m x2))) (B, ((tau 3 (m x2)), n ^^ nb)) (m x4)) then acc else O).
  
Definition qa2_s :=   (If ((to x3) #?(i 1)) & (ver (pkey (m x1)) (A, ((pi1 (m x3)), n ^^ na)) (pi2 (m x3))) then qa3 else O).

Definition qa1_ss :=   (If ((to x2)#?(i 2))& (check (tau 1 (m x2))) & ((tau 2 (m x2)) #? B) then qa2_s else O).
Definition qa0_sss :=   (If ((to x1) #? (i 1)) & ((act x1)#? new) & (check (m x1)) then qa1_ss else O).
 (** if there is no matching session for a completed responder session, the oracle outputs [AF] indicating that there is an attack, other wise outputs [ADE] *) 
  Definition phi14 := phi13 ++ [msg acc]. 
 Definition mphi14 := (conv_mylist_listm phi13).
 Definition x5 := (f mphi14).
 
  Definition oracleop1 :=  (If ((reveal x5)#?(i 2))& ((tau 1 (m x2)) #? A) & (ver (pkey (tau 1 (m x2))) (B, ((tau 3 (m x2)), n ^^ nb)) (m x4))  & (! (((pi1 (m x3))#? n ^^ nb) & ((tau 3 (m x2)) #? n ^^ na) & ((m x1) #? B))) then AF else ADE).
  
Definition qa3_s :=  (If ((to x4)#? (i 2)) & (ver (pkey (tau 1 (m x2))) (B, ((tau 3 (m x2)), n ^^ nb)) (m x4)) then oracleop1 else O).
  
Definition qa2_ss :=  (If ((to x3) #?(i 1)) & (ver (pkey (m x1)) (A, ((pi1 (m x3)), n ^^ na)) (pi2 (m x3))) then qa3_s else O).

Definition qa1_sss :=   (If ((to x2)#?(i 2))& (check (tau 1 (m x2))) & ((tau 2 (m x2)) #? B) then qa2_ss else O).
Definition qa0_ssss :=   (If ((to x1) #? (i 1)) & ((act x1)#? new) & (check (m x1)) then qa1_sss else O).
 
(** Frame [phi13] attacker's knowledge at the end of the protocol. *)


Definition phi15 := phi14 ++ [msg qa0_ssss].

(** Protocol Pi2 reveals [new] if there is an attack, reveals [acc] otherwise. 

 All the frames in this protocol are equal to the frames in Pi1 except the last one and we define it here.
 *)

(** The oracle always outputs [ADE] *)
  Definition oracleop2 :=  (If ((reveal x5)#?(i 2)) then ADE else ADE).

 
Definition qb3_s :=  (If ((to x4)#? (i 2)) & (ver (pkey (tau 1 (m x2))) (B, ((tau 3 (m x2)), n ^^ nb)) (m x4)) then oracleop2 else O).
  
Definition qb2_ss :=  (If ((to x3) #?(i 1)) & (ver (pkey (m x1)) (A, ((pi1 (m x3)), n ^^ na)) (pi2 (m x3))) then qb3_s else O).

Definition qb1_sss :=   (If ((to x2)#?(i 2))& (check (tau 1 (m x2))) & ((tau 2 (m x2)) #? B) then qb2_ss else O).
Definition qb0_ssss :=   (If ((to x1) #? (i 1)) & ((act x1)#? new) & (check (m x1)) then qb1_sss else O).
 
Definition phi25 := phi14 ++ [msg qb0_ssss].

Theorem EQm_ref: forall m, m#?m ## TRue.
Proof. intros.
apply EQmsg. reflexivity.
Qed.
(** Proof of [phi13 #### phi23], where [####] is an equivalence relations on mylists. *)
Check forallb.
SearchAbout beq_nat.
Fixpoint exp_nat_app (n:nat) (l:Nlist) : Prop:=
  match l with
  | [ ] => True
  | h::t => (n =? h) = false /\ (exp_nat_app n t)
  end.
Fixpoint ilist_to_list {A} {n}(l: ilist A n): list A :=
  match l with
  | [] => [ ]
  | h : t => h :: (ilist_to_list t)
  end.

(** few tactics *)

Ltac clear_T :=
  repeat match goal with
         | [H: True |- _ ] => clear H
                                    
       end.

  Ltac rep_inv :=
 repeat match goal with
        | [H: _ /\ _ |- _] => inversion H; clear H; clear_T
                                                 
        end.

  
Ltac rew_hyp_false :=
  repeat match goal with
         | [H: ?X =? ?Y = false |- context[?X =? ?Y] ] => rewrite H 
         end.

Ltac rew_hyp_sym_false :=
  repeat match goal with
         | [H: ?X =? ?Y = false |- context[?Y =? ?X] ] => rewrite PeanoNat.Nat.eqb_sym; rewrite H 
         end.

Theorem IND_DH_AUTH:
  let l := [n, nA, nB, na, nb, n1, n2, c1, c2] in
  let p:= (fix exp_nat_app' (l2: Nlist) :=
             match l2 with
             | [ ] => True
             | h :: t => (exp_nat_app h t)/\ (exp_nat_app' t)
             end)
             in
  p (ilist_to_list l) -> phi15 #### phi25.
                                                                  
Proof. intros. simpl in H.


       repeat unfold phi11, phi12, phi13, phi14, phi15, phi25. repeat  unfold qa0, qa1 , qa2, qa3,  qa0_s, qa1_s, qa2_s, qa3_s. simpl. 
repeat unfold qa0_ss, qa0_sss, qa0_ssss, qb0_ssss, qa1_ss, qa1_sss, qb1_sss, qa2_s, qb2_ss, qa2_ss, qa3_s. 
repeat unfold oracleop1, oracleop2. 
repeat unfold qb3_s, qa1_s, qa3, qa2.
unfold oracleop2.

assert (qa0_ssss # qb0_ssss). 
repeat unfold qa0_ssss, qa1_sss, qa2_ss, qa3_s, qa3, qb0_ssss, qb1_sss, qb2_ss, qb3_s.
unfold oracleop1, oracleop2.
unfold andB. pose proof(eqbrmsg_msg'').
fold (((tau 3 (m x2)) #? (n) ^^ (na)) & (m x1) #? B).
fold ((pi1 (m x3)) #?(n) ^^ (nb)) & ((tau 3 (m x2)) #? (n) ^^ (na)) & (m x1) #? B.
fold (ver (pkey (tau 1 (m x2))) (B, (tau 3 (m x2), (n) ^^ (nb))) (m x4)) & (! (((pi1 (m x3)) #?(n) ^^ (nb)) & ((tau 3 (m x2)) #? (n) ^^ (na)) & (m x1) #? B)).                           
pose proof(eqbrmsg_msg'' (tau 1 (m x2)) (tau 1 (m x2)) A  (ver (pkey (tau 1 (m x2))) (B, (tau 3 (m x2), (n) ^^ (nb))) (m x4)) & (! (((pi1 (m x3)) #?(n) ^^ (nb)) & ((tau 3 (m x2)) #? (n) ^^ (na)) & (m x1) #? B))).
simpl in H1.
repeat rewrite <-beq_nat_refl in H1;simpl in H1.  repeat rewrite <- beq_nat_refl in H1; red_in H1. fold (G n) (g n) (r na) (r nb) in H1. fold (pk nA) (pk nB) in H1.
rewrite H1 ; clear H1.
pose proof(EQmsg A A).
assert(A#A); try reflexivity. apply H1 in H2; rewrite H2. rewrite IFTRUE_M. fold x1 x2 x3 x4. fold (pk nA).
  
  fold (tau 3 (m x2)). 
repeat fold (G n) (g n) (pk nA) (pk nB) phi10 (r na) (r nb) (sk nA) (sk nB). fold  mphi10.
fold x1 x2 x3 x4. fold (tau 3 (m x2)). fold x1.
Eval compute in x2.
assert ( (B, (tau 3 (m x2), (n) ^^ (nb))) # (B, (pi2 (pi2 (m (f [G n; g n; A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2; (A, (m (f [G n; g n; A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2]), (n) ^^ (na)))]))), (n) ^^ (nb)))).
unfold x2. 
simpl. unfold mphi11. unfold phi11. simpl.
 reflexivity. fold (pk c1) (pk c2).
rewrite <- H3. Eval compute in x4.

assert ((f [G n; g n; A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2]) # x1).
reflexivity.
repeat rewrite H4. fold x2.
assert((f [G n; g n;  A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2; (A, (m x1, (n) ^^ (na)))]) # x2).
reflexivity. 
repeat rewrite H5.
Definition x4':= (f [G n; g n; A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2; (A, (m x1, (n) ^^ (na))); ((n) ^^ (nb), sign (sk nB) (A, ((n) ^^ (nb), pi2 (pi2 (m x2)))) (rr (N n1))); sign (sk nA) (m x1, ((n) ^^ (na), pi1 (m (f [G n; g n;  A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2; (A, (m x1, (n) ^^ (na))); ((n) ^^ (nb), sign (sk nB) (A, ((n) ^^ (nb), pi2 (pi2 (m x2)))) (rr (N n1)))])))) (rr (N n2))]).
                                                           
        
pose proof( UFCMA nA (B, (tau 3 (m x2), (n) ^^ (nb))) (m x4')).

rewrite H6. simpl.
repeat rewrite <- beq_nat_refl. redg.
  rep_inv.
 rew_hyp_false. simpl.
                                                                   
SearchAbout beq_nat.                                                                    

rew_hyp_sym_false. simpl.
unfold distsigntrms. unfold list_skn_in_sign. simpl.
repeat rewrite <- beq_nat_refl.
rew_hyp_sym_false. simpl.

Definition x3':= (f [G n; g n;  A; pk nA; B; pk nB; Q1; pk c1; Q2; pk c2; (A, (m x1, (n) ^^ (na))); ((n) ^^ (nb), sign (sk nB) (A, ((n) ^^ (nb), pi2 (pi2 (m x2)))) (rr (N n1)))]).
                                                              
                                                             
assert((tau 1 ((m x1), (n^^na, (pi1 (m x3'))))) #? (tau 1 (B, ((pi2 (pi2 (m (f mphi11)))), n^^nb))) ## ((m x1) #? B)).
simpl. repeat rewrite proj1.  reflexivity.
assert( ((pi2 (pi2 (m (f mphi11)))) #? n ^^ na) ##  ((tau 2 ((m x1), (n^^na, (pi1 (m x3'))))) #? (tau 2 (B, ((pi2 (pi2 (m (f mphi11)))), n^^nb))))). simpl.
repeat rewrite proj2.
repeat rewrite proj1. simpl.
rewrite Example14_M'. reflexivity. 
assert ( (tau 3 ((m x1), (n^^na, (pi1 (m x3'))))) #? (tau 3 (B, ((pi2 (pi2 (m (f mphi11)))), n^^nb))) ## ((pi1 (m x3')) #? n^^nb)).
simpl.
repeat rewrite proj2. reflexivity.
rewrite <-H42.
rewrite H43.
rewrite <- H44. 
fold ( ((tau 2 (m x1, ((n) ^^ (na), pi1 (m x3')))) #? (tau 2 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) & (tau 1 (m x1, ((n) ^^ (na), pi1 (m x3')))) #? (tau 1 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))).
                                                                  
fold ((tau 3 (m x1, ((n) ^^ (na), pi1 (m x3')))) #? (tau 3 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb)))))& ((tau 2 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                              (tau 2 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) &
                                                             (tau 1 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                             (tau 1 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb)))).
fold (! (((tau 3 (m x1, ((n) ^^ (na), pi1 (m x3')))) #? (tau 3 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb)))))& ((tau 2 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                              (tau 2 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) &
                                                             ((tau 1 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                             (tau 1 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))))).
fold x3'.
rewrite andB_comm with ( b1:=((B, (pi2 (pi2 (m x2)), (n) ^^ (nb)))) #? (m x1, ((n) ^^ (na), pi1 (m x3')))) (b2:= ver (pk nA) (m x1, ((n) ^^ (na), pi1 (m x3'))) (m x4')).
rewrite <- IFSAME_B with (b:= (ver (pk nA) (m x1, ((n) ^^ (na), pi1 (m x3'))) (m x4'))) (b1:= IF (ver (pk nA) (m x1, ((n) ^^ (na), pi1 (m x3'))) (m x4')) &
                                                   ((B, (pi2 (pi2 (m x2)), (n) ^^ (nb)))) #? (m x1, ((n) ^^ (na), pi1 (m x3')))
                                                then ! (((tau 3 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                         (tau 3 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) &
                                                        ((tau 2 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                         (tau 2 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) &
                                                        (tau 1 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                        (tau 1 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) else FAlse).
 rewrite IFEVAL_B' with (b:= ver (pk nA) (m x1, ((n) ^^ (na), pi1 (m x3'))) (m x4')).
 simpl.
repeat rewrite <- beq_nat_refl. simpl. repeat redg. fold x3'. fold x4' (G n) (g n) x1 x2 x3' (r na) (r nb).

 
pose proof(eqbrmsg_msg'' ((B, (pi2 (pi2 (m x2)), (n) ^^ (nb)))) ((B, (pi2 (pi2 (m x2)), (n) ^^ (nb)))) (m x1, ((n) ^^ (na), pi1 (m x3')))  ! (((tau 3 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                         (tau 3 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) &
                                                        ((tau 2 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                         (tau 2 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) &
                                                        (tau 1 (m x1, ((n) ^^ (na), pi1 (m x3')))) #?
                                                        (tau 1 (B, (pi2 (pi2 (m (f mphi11))), (n) ^^ (nb))))) FAlse). 
simpl in H45.
repeat rewrite <- beq_nat_refl in H45. simpl in H45. repeat fold x1 x2 x3' x4' (G n) (g n) (r na) (r nb) in H45.
rewrite H45.

repeat rewrite EQm_ref.  repeat redg.  reflexivity. 
split; auto. simpl. 
rep_inv;rew_hyp_false; rew_hyp_sym_false; auto. rewrite H0; auto.
reflexivity.

Qed.

End dh_auth.