(************************************************************************)
(* Copyright (c) 2015-2019, Ajay Kumar Eeralla                          *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)    

Require Export ex7_7.
(**  This library defines a theorem that states, [if not(b) then x else y = if b then y else x ].
(*<!-- Of course, we use ## (resp. ##) for message (resp. Bool) in lieu of [=]. -->*) *)

Theorem Example16_B :  forall (n:nat) (b1 b2 :Bool), (IF (notb (Bvar n)) then b1 else b2) ## (IF (Bvar n) then b2 else b1).
Proof.
intros.
unfold notb.
assert(H1: (IF (IF (Bvar n) then FAlse else TRue) then b1 else b2) ##
    (IF (Bvar n) then (IF FAlse then b1 else b2) else (IF TRue then b1 else b2))).
apply IFMORPH_B2 .
rewrite H1.
rewrite IFFALSE_B.
rewrite IFTRUE_B.
reflexivity.
Qed.
Theorem Example16_M :  forall (n:nat) (m1 m2:message), (If (notb (Bvar n)) then m1 else m2) # (If (Bvar n) then m2 else m1).
Proof.
intros.
unfold notb.
assert(H1: (If (IF (Bvar n) then FAlse else TRue) then m1 else m2) #
    (If (Bvar n) then (If FAlse then m1 else m2) else (If TRue then m1 else m2))).
apply IFMORPH_M2 .
rewrite H1.
rewrite IFFALSE_M.
rewrite IFTRUE_M.
reflexivity.
Qed.
